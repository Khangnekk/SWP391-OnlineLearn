<!-- <%-- 
    Document   : course
    Created on : Feb 1, 2023, 12:06:56 AM
    Author     : Khangnekk
--%> -->

 <%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Các khóa học</title>
        <link rel="stylesheet" href="../Assets/css/Style.css">
        <link rel="stylesheet" href="../Assets/css/course.css">
        <!-- link bootstrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
    <body>
        <div class="main">
        <div class="nav">
            <div class="nav-bar">
                <ul class="items">
                    <li class="item">
                        <a href="#"><img src="../Assets/icon/icon (69).png"></a>
                    </li>
                    <li class="item">
                        <a href="#"><img src="../Assets/icon/icon (44).png"></a>
                    </li>
                    <li class="item">
                        <a href="classes.html"><img src="../Assets/icon/icon (71).png"></a>
                    </li>
                    <li class="item">
                        <a href="#"><img src="../Assets/icon/icon (51).png"></a>
                    </li>
                    <li class="item">
                        <a href="#"><img src="../Assets/icon/icon (45).png"></a>
                    </li>
                    <li class="item avt-btn">
                        <a href="#"><img src="../Assets/icon/icon (4).png">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content">
            <div class="container-fluid">
                <ul class="">
                    <li class="item-course">
                        <h1>Chỗ này là chỗ để tên của cái course đấy</h1>
                        <p>Chỗ này là mô tả về những gì sẽ học được từ course này Chỗ này là
                            mô tả về những gì sẽ học được từ course này Chỗ này là mô tả về những gì sẽ học được từ
                            course này</p>
                        <a href="modules.jsp">
                            <button class="go-to-course-btn">
                                Go to course
                            </button>
                        </a>
                    </li>
                    <li class="item-course">
                        <h1>Chỗ này là chỗ để tên của cái course đấy (Vip)</h1>
                        <p>Chỗ này là mô tả về những gì sẽ học được từ course này Chỗ này là
                            mô tả về những gì sẽ học được từ course này Chỗ này là mô tả về những gì sẽ học được từ
                            course này</p>
                        <a href="module.html">
                            <button class="go-to-course-btn">
                                Go to course
                            </button>
                        </a>
                    </li>
                    <li class="item-course">
                        <h1>Chỗ này là chỗ để tên của cái course đấy</h1>
                        <p>Chỗ này là mô tả về những gì sẽ học được từ course này Chỗ này là
                            mô tả về những gì sẽ học được từ course này Chỗ này là mô tả về những gì sẽ học được từ
                            course này</p>
                        <a href="module.html">
                            <button class="go-to-course-btn">
                                Go to course
                            </button>
                        </a>
                    </li>
                    <li class="item-course">
                        <h1>Chỗ này là chỗ để tên của cái course đấy</h1>
                        <p>Chỗ này là mô tả về những gì sẽ học được từ course này Chỗ này là
                            mô tả về những gì sẽ học được từ course này Chỗ này là mô tả về những gì sẽ học được từ
                            course này</p>
                        <a href="module.html">
                            <button class="go-to-course-btn">
                                Go to course
                            </button>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    </body>
</html>
